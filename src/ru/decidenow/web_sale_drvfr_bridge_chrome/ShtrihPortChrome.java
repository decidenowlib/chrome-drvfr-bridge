package ru.decidenow.web_sale_drvfr_bridge_chrome;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.ini4j.Ini;
import org.json.JSONException;
import org.json.JSONObject;

public class ShtrihPortChrome {

	
	protected static Logger logger = null;
	
	public static void initLogger() {
		
		File param_file = new File("param.ini");
		if (!param_file.exists()) {
			return;
		}
		try {
			Ini ini = new Ini(new File("param.ini"));
			String param_log = ini.get("common", "log");
			if (!param_log.equals("1")) {
				return;
			}
		} catch (IOException e1) {
			return;
		}
		
		logger = Logger.getLogger("DriverLog");
				
		FileHandler fh;
		try {
			Handler[] handlers = logger.getHandlers();
			for (Handler handler : handlers) {
				logger.removeHandler(handler);
			}
			fh = new FileHandler("LogFile.log");
			fh.setFormatter(new SimpleFormatter());
			logger.addHandler(fh);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void loggerInfo(String message) {
		if (logger != null) {
			logger.info(message);
		}
	}
	
	public static void loggerWarning(String message) {
		if (logger != null) {
			logger.warning(message);
		}
	}

	
	public static void main(String[] args) throws Exception {

		initLogger();
		loggerInfo("Start");

		// Connect to COM-Object
		ShtrihProtocolChrome protocol = new ShtrihProtocolChrome();
		ShtrihProtocolResult responseObj = protocol.initCOMObject();
		if (responseObj.getCode() < 0) {
			JSONObject responseJSON = responseObj.toObjectJSON();
			String response = responseJSON.toString();
			loggerWarning(response);
			loggerInfo("Stop");
			return;
		}

		// Read messages...
		String requestStringJson = readMessage(System.in);
		while (requestStringJson != "") {

			// format JSON request
			JSONObject requestObj = new JSONObject();
			try {
				requestObj = new JSONObject(requestStringJson);
			} catch (JSONException e) {
				if (requestStringJson == "*") {
					loggerInfo("Stop");
					return;
				}
				loggerWarning("Wrong message format: " + requestStringJson + "!");
				requestStringJson = readMessage(System.in);
				continue;
			}

			// read JSON request
			String command = requestObj.getString("command");
			String sender_id = requestObj.getString("sender_id");

			HashMap<String, Object> params = new HashMap<String, Object>();

			JSONObject paramsJSON = requestObj.getJSONObject("params");
			if (paramsJSON.length() > 0) {
				for (int i = 0; i < paramsJSON.names().length(); i++) {
					String key = paramsJSON.names().getString(i);
					Object val = paramsJSON.get(key);
					params.put(key, val);
				}
			}

			if (command == "Exit") {
				loggerInfo("Stop");
				return;
			}

			// Generate response
			String response = "";
			JSONObject responseJSON = new JSONObject();
			try {
				ShtrihProtocolResult res = protocol.exec(command, params);
				responseJSON = res.toObjectJSON();
			} catch (JSONException e) {
				responseJSON.put("code", -100);
				responseJSON.put("codeDescription", e.getMessage());
			}

			responseJSON.put("command", command);
			responseJSON.put("sender_id", sender_id);
			responseJSON.put("params", paramsJSON);
			
			response = responseJSON.toString();
			
			sendMessage(response);

			requestStringJson = readMessage(System.in);
		}
		loggerInfo("Stop");
	}

	
	private static String readMessage(InputStream in) throws IOException {

		// Read message size
		byte[] sz = new byte[4];
		in.read(sz);
		int size = getInt(sz);

		// Read message
		String message = "-";
		if (size == 0 || size == 2573) {
			loggerWarning("Message empty!");
			return "*";
		}
		if (size > 4096) {
			loggerWarning("Message too long!");
			return "-";
		}
		
		byte[] msg = new byte[size];
		in.read(msg);
		message = new String(msg, "UTF-8");

		message = message.trim().replace("\n", "").replace("\r", "");
		
		loggerInfo(" -> " + message);
		return message;
	}

	private static void sendMessage(String message) throws IOException {

		loggerInfo(" <- " + message);

		byte[] msg = message.getBytes("UTF-8");
		System.out.write(getBytes(msg.length));
		System.out.write(msg);
		System.out.flush();
	}

	public static int getInt(byte[] bytes) {
		return (bytes[3] << 24) & 0xff000000 | (bytes[2] << 16) & 0x00ff0000 | (bytes[1] << 8) & 0x0000ff00
				| (bytes[0] << 0) & 0x000000ff;
	}

	public static byte[] getBytes(int length) {
		byte[] bytes = new byte[4];
		bytes[0] = (byte) (length & 0xFF);
		bytes[1] = (byte) ((length >> 8) & 0xFF);
		bytes[2] = (byte) ((length >> 16) & 0xFF);
		bytes[3] = (byte) ((length >> 24) & 0xFF);
		return bytes;
	}

}
