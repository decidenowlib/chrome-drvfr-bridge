package ru.decidenow.web_sale_drvfr_bridge_chrome;

import java.io.File;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import org.ini4j.Ini;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.Variant;

public class ShtrihProtocolChrome {

	protected ActiveXComponent drv;
	protected int drvPassword;
	protected String drvComponentId = "Addin.DRvFR";

	public ShtrihProtocolChrome() {
		//
	}
	
	public ShtrihProtocolResult initCOMObject() {
		ShtrihProtocolResult ret = new ShtrihProtocolResult();
		try {
			this.drv = new ActiveXComponent(this.drvComponentId);
			this.drvPassword = (int) this.getProperty("Password", "int");
		} catch (Exception e) {
			ret.setResult(-1, e.getMessage());
		}
		return ret;
	}

	public Object getProperty(String property, String type) {
		Object ret = null;
		Variant v = drv.getProperty(property);
		if (type.equals("int")) {
			ret = v.getInt();
		}
		if (type.equals("string")) {
			ret = v.getString();
		}
		if (type.equals("double")) {
			ret = v.getDouble();
		}
		if (type.equals("date")) {
			ret = v.getDate();
		}
		return ret;
	}

	public void setProperty(String property, Object value) {
		drv.setProperty(property, new Variant(value));
	}
	
	public int getResultCode() {
		return (int) this.getProperty("ResultCode", "int");
	}

	public String getResultCodeDescription() {
		return (String) this.getProperty("ResultCodeDescription", "string");
	}

	public ShtrihProtocolResult getResult() {
		return new ShtrihProtocolResult(this.getResultCode(), this.getResultCodeDescription());
	}

	public ShtrihProtocolResult invoke(String command) {
		this.setProperty("Password", this.drvPassword);
		drv.invoke(command);
		return this.getResult();
	}
	
	public String GetHostSerialNumber() {
		String ret = "";
		try {
			Ini ini = new Ini(new File("param.ini"));
			String param_pos_serial = ini.get("common", "pos_serial");
			if (param_pos_serial != null) {
				ret = param_pos_serial.trim();
			}
		} catch (Exception e1) {
			ShtrihPortChrome.loggerWarning(e1.getMessage());
			return "";
		}
		return ret;
	}
	
	public String GetHostCredentials() {
		String ret = "";
		try {
			Ini ini = new Ini(new File("param.ini"));
			String param_credentials = ini.get("common", "credentials");
			if (param_credentials != null) {
				ret = Base64.getEncoder().encodeToString(param_credentials.trim().getBytes());
			}
		} catch (Exception e1) {
			ShtrihPortChrome.loggerWarning(e1.getMessage());
			return "";
		}
		return ret;
	}
	
	
	public ShtrihProtocolResult exec(String command, HashMap<String, Object> params)
	{
		ShtrihProtocolResult ret = new ShtrihProtocolResult();
		
		switch (command) {
			case "GetSerialNumber":
				ret = this.invoke("GetECRStatus");
				if (ret.getCode() == 0) {
					String val = this.getProperty("SerialNumber", "string").toString();
					ret.setResult(0, val);
				}
				break;
			case "GetECRMode":
				ret = this.invoke("GetECRStatus");
				if (ret.getCode() == 0) {
					String val = this.getProperty("ECRMode", "int").toString();
					ret.setResult(0, val);
				}
				break;
			case "SetCashierITN":
				String ITN = params.get("ITN").toString();
				this.setProperty("TagNumber", 1203);
				this.setProperty("TagType", 7);
				this.setProperty("TagValueStr", ITN);
				this.setProperty("StringForPrinting", "");
				ret = this.invoke("FNSendTag");
				break;
			case "SetCashierName":
				String Name = params.get("Name").toString();
				this.setProperty("TagNumber", 1021);
				this.setProperty("TagType", 7);
				this.setProperty("TagValueStr", Name);
				this.setProperty("StringForPrinting", "");
				ret = this.invoke("FNSendTag");
				break;
			case "GetHostSerialNumber":
				String posSerial = this.GetHostSerialNumber();
				ret.setResult(0, posSerial);
				break;
			case "GetHostCredentials":
				String credentials = this.GetHostCredentials();
				ret.setResult(0, credentials);
				break;
			default:
				for (Map.Entry<String, Object> param : params.entrySet() ) {
					this.setProperty(param.getKey(), param.getValue());
				}
				ret = this.invoke(command);
				break;
		}
		return ret;
	}

}
