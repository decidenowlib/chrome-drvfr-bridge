package ru.decidenow.web_sale_drvfr_bridge_chrome;

import org.json.JSONException;
import org.json.JSONObject;

public class ShtrihProtocolResult {
	private int code = 0;
	private String codeDescription = "";

	public ShtrihProtocolResult() {
		this.setResult(0, "");
	}

	public ShtrihProtocolResult(int code, String codeDescription) {
		this.setResult(code, codeDescription);
	}

	public void setResult(int code, String codeDescription) {
		this.setCode(code);
		this.setCodeDescription(codeDescription);
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getCodeDescription() {
		return codeDescription;
	}

	public void setCodeDescription(String codeDescription) {
		this.codeDescription = codeDescription;
	}

	public String toString() {
		return "Code " + String.valueOf(this.code) + ": " + this.codeDescription;
	}
	
	public JSONObject toObjectJSON() throws JSONException
	{
		JSONObject ret = new JSONObject();
		ret.put("code", this.code);
		ret.put("codeDescription", this.codeDescription);
		return ret;
	}
	
	public String toStringJSON() throws JSONException
	{
		JSONObject obj = this.toObjectJSON();
		return obj.toString();
	}

}
