@echo off 
for /d %%d in ("C:\Program Files (x86)\Java\jre*") do @set bin_path=%%d\bin\
if exist "%bin_path%jacob-1.19-x86.dll" (
    rem do nothing
) else (
    rem copy library
	copy "%~dp0jacob-1.19-x86.dll" "%bin_path%jacob-1.19-x86.dll" > NUL
)
@set jpath=%bin_path%java.exe
"%jpath%" -jar "%~dp0host_chrome_drvfr_bridge.jar" %*
