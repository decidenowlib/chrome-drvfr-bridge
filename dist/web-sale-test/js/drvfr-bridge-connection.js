
DrvFRCommandBatch = function(connection) {
	
	this.commands = [];
	this.is_active = false;
	this.active_command = 0;
	this.onSuccess = null;
	this.onError = null;
	this.context = null;
	
	this.add = function (command, params) {
		params = (typeof params === 'undefined') ? {} : params;
		this.commands.push({ 'command': command, 'params': params });
	}
	
	this.clear = function () {
		this.commands = [];
	}
	
	this.start = function (onSuccess, onError, context) {
		this.onSuccess = onSuccess;
		this.onError = onError;
		this.context = context;
		this.is_active = true;
		this.active_command = 0;
		
		this.next();
	}
	
	this.stop = function (response) {
		this.is_active = false;
		this.active_command = 0;
		this.commands = [];
	}
	
	this.next = function (response) {
		if (!this.is_active) {
			return false;
		}
		if (this.commands.length > this.active_command) {
			connection.postMessage(this.commands[this.active_command]);
			this.active_command++;
		} else {
			this.stop(response);
			return true;
		}
		return false;
	}
	
	this.onExtensionResponse = function (response) {
		if (response.data.code != 0) {
			this.stop(response);
			return true;
		} else {
			return this.next(response);
		}
	}
}

DrvFRBridgeConnection = function (onExtensionResponse) {
	this.extensionId = 'cobmeiimbobabhdkfdchekpkealnncal';
	this.extensionPort = null;
	 
	
	this.init = function (thisObj) {
		if (typeof ShtihPort === 'undefined') {
			var port = chrome.runtime.connect(this.extensionId);
			port.onMessage.addListener(this.onMessage);
			port.onDisconnect.addListener(this.onDisconnect);
			this.extensionPort = port;
			console.log('Shtrih Bridge: Start Chrome');
		} else {
			window.ShtihPortJS = {};
			window.ShtihPortJS.onMessage = function (response) {
				thisObj.onMessage(response);
			};
			window.ShtihPortJS.postMessage = function (request) {
				ShtihPort.postMessage(request);
			};
			this.extensionPort = ShtihPortJS;
			console.log('Shtrih Bridge: Start Android');
		}
		
	}
	
	this.onMessage = function (response) {
		if (typeof ShtihPort !== 'undefined') {
			console.log(response);
			response = JSON.parse(response);
			var msg = { command: response.command };
			delete response.command;
			msg.data = response;
			response = msg;
		}
		onExtensionResponse(response);
	}

	this.onDisconnect = function (response) {
		response = { data: {code: '-100', codeDescription: 'Не установлено соединение с расширением "Chrome DrvFR Bridge"' } };
		onExtensionResponse(response);
		this.extensionPort = null;
	}
	
	this.postMessage = function (request) {
		if (this.extensionPort == null) {
			this.init(this);
		}
		if (typeof ShtihPort !== 'undefined') {
			request = JSON.stringify(request);
			console.log(request);
			this.extensionPort.postMessage(request);
		} else {
			try {
				this.extensionPort.postMessage(request);
			} catch (err) {
				response = { data: {code: '-100', codeDescription: 'Не установлено соединение с расширением "Chrome DrvFR Bridge"' } };
				onExtensionResponse(response);
			}
		}
	}
	
	this.toInt = function(val) {
		var ret = parseInt(val);
		if (isNaN(ret)) { ret = 0 };
		return ret;
	}
	
	this.toDecimal = function(val) {
		var ret = parseFloat(val);
		if (isNaN(ret)) { ret = 0 };
		return ret;
	}
}

