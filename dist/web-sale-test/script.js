
var extensionConnection = new DrvFRBridgeConnection(function (response) {
	$('#result').val(response.data.code);
	$('#result_description').val(response.data.codeDescription);
});

var toInt = function(val) {
	var ret = parseInt(val);
	if (isNaN(ret)) { ret = 0 };
	return ret;
}
var toDouble = function(val) {
	var ret = parseFloat(val);
	if (isNaN(ret)) { ret = 0 };
	return ret;
}

var initDocument = function() {
	$('#btn_connect').click(function(e) {
		extensionConnection.postMessage({
			command: 'Connect',
			params: {}
		});
		
	});
	$('#btn_disconnect').click(function(e) {
		extensionConnection.postMessage({
			command: 'Disconnect',
			params: {}
		});
		
	});
	$('#btn_settings').click(function(e) {
		extensionConnection.postMessage({
			command: 'ShowProperties',
			params: {}
		});
	});
	$('#btn_beep').click(function(e) {
		extensionConnection.postMessage({
			command: 'Beep',
			params: {}
		});
	});
	
	$('#btn_sale').click(function(e) {
		var Quantity = toDouble($('#quantity').val());
		var Price = toDouble($('#price').val());
		var Department = toInt($('#department').val());
		var Name = $('#name').val();
		extensionConnection.postMessage({
			command: 'Sale',
			params: {
				'Quantity': Quantity,
				'Price': Price,        
				'Department': Department,
				'StringForPrinting': Name
			}
		});
	});
	
	$('#btn_receipt_close').click(function(e) {
		var Summ1 = toDouble($('#sum1').val());
		var Summ2 = toDouble($('#sum2').val());
		var Summ3 = toDouble($('#sum3').val());
		var Summ4 = toDouble($('#sum4').val());
		var DiscountOnCheck = toDouble($('#receipt_discount').val());
		var Caption = $('#caption').val();
		extensionConnection.postMessage({
			command: 'CloseCheck',
			params: {
				'Summ1': Summ1, 
				'Summ2': Summ2,
				'Summ3': Summ3,
				'Summ4': Summ4,
				'DiscountOnCheck': DiscountOnCheck,
				'StringForPrinting': Caption
			}
		});
	});
	
	$('#btn_ecrmode').click(function(e) {
		extensionConnection.postMessage({
			command: 'GetProperty',
			params: {
				'name': 'ECRMode', 
				'type': 'int'
			}
		});
	});
}
	
$(document).ready(function () {
	initDocument();
});
