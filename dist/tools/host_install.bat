@echo off

@set hpath=%~dp0
for /d %%d in ("%~dp0..\host-application") do @set hpath=%%~fd
REG ADD "HKLM\Software\Google\Chrome\NativeMessagingHosts\ru.decidenow.chrome_drvfr_bridge" /ve /t REG_SZ /d "%hpath%/ru.decidenow.host_chrome_drvfr_bridge.json" /f

@set jpath = "no_such_file"
for /d %%d in ("C:\Program Files (x86)\Java\jre*") do @set jpath=%%d\bin\java.exe
if exist "%jpath%" (
    rem do nothing
) else (
    rem copy library
	echo "ERROR: Java x86 not installed"
	pause
)

@set shpath = "no_such_file"
for /d %%d in ("C:\Program Files (x86)\SHTRIH-M\DrvFR*") do @set shpath=%%d\bin\DrvFR.dll
if exist "%shpath%" (
    rem do nothing
) else (
    rem copy library
	echo "ERROR: SHTRIH-M Driver not installed"
	pause
)
