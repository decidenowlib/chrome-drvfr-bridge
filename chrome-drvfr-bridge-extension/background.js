
var popupStatusList = ['connected', 'disconnected', 'error'];
var popupStatus = 'error';

var driverPort = null;
var driverHostName = 'ru.decidenow.chrome_drvfr_bridge';
var driverLog = [];

var externalPorts = {};


/* Popup */

var popupStatus_update = function () {
	var iconPath = 'icons/status_' + popupStatus + '-128.png';
	chrome.browserAction.setIcon({
		path: iconPath
	});
	
	var views = chrome.extension.getViews({
		type: 'popup'
	});
	for (var i = 0; i < views.length; i++) {
		views[i].document.getElementById('status-image').src = '../' + iconPath;
		if (popupStatus === 'error') {
			views[i].document.getElementById('btn-init-driver').classList.remove('hidden');
		} else {
			views[i].document.getElementById('btn-init-driver').classList.add('hidden');
		}
	}
}

var popupStatus_set = function (status) {
	status = (popupStatusList.indexOf(status) == -1) ? 'error' : status;
	popupStatus = status;
	popupStatus_update();
}

var runtime_onMessage = function (request, sender, sendResponse) {
	if (!('command' in request)) {
		return;
	}
		
	if (request.command === 'getPopupStatus') {
		sendResponse({ command: request.command, data: { status: popupStatus } });
	}
	
	if (request.command === 'getDriverLog') {
		sendResponse({ command: request.command, data: { log: driverLog } });
	}
	
	if (request.command === 'driverInit') {
		driverPort_init();
		sendResponse({ command: request.command, data: { result: 'ok' } });
	}
	
	if (request.command === 'pagesDisconnect') {
		for (port_key in externalPorts) {
			externalPorts[port_key].disconnect();
		}
		sendResponse({ command: request.command, data: { result: 'ok', message: 'Страницы отсоединены.' } });
	}
}


/* Driver (Native Messaging) */

var driverLog_MessageToString = function (msg) {
	log_message = (typeof msg === 'string') ? (msg) : (msg.command + ': Код ' + msg.code + ' => ' + msg.codeDescription);
	return log_message;
}

var driverLog_push = function (msg, msg_status) {
	log_message = driverLog_MessageToString(msg);
	driverLog.push({ message: log_message, message_status: msg_status });
	if (msg.sender_id) {
		var port = externalPorts[msg.sender_id];
		externalPort_postMessage(msg, port);
	}
}

var driverPort_onMessage = function (response) {
	var msg_status = (response.code != 0) ? 'error' : 'normal';
	
	if (response.command == 'Disconnect') {
		popupStatus_set('disconnected');
	}
	if (response.command == 'Connect') {
		if (response.code != 0) {
			popupStatus_set('disconnected');
		} else {
			popupStatus_set('connected');
		}
	}
	
	driverLog_push(response, msg_status);
}
var driverPort_onDisconnect = function () {
	driverPort = null;
	popupStatus_set('error');
	driverLog_push('Disconnected: ' + chrome.runtime.lastError.message, 'error');
}

var  driverPort_init = function () {
	driverPort = chrome.runtime.connectNative(driverHostName);
	driverPort.onMessage.addListener(driverPort_onMessage);
	driverPort.onDisconnect.addListener(driverPort_onDisconnect);
	popupStatus_set('disconnected');
}


/* External (page) */

var externalPort_onMessage = function (request, port) {
	if (driverPort === null) {
		popupStatus_set('error');
		alert('Отсутствует соединение с драйвером!');
		return;
	}
	request.sender_id = port.sender.tab.id;
	driverPort.postMessage(request);
}

var externalPort_onConnect = function (port) {
	//if (!(port.sender.tab.id in externalPorts)) {
	//}
	externalPorts[port.sender.tab.id] = port;
	port.onMessage.addListener(externalPort_onMessage);
}

var externalPort_postMessage = function(response, port) {
	var msg = { command: response.command };
	delete response.command;
	delete response.sender_id;
	msg.data = response;
	port.postMessage(msg);
}


/* Init */

driverPort_init();
chrome.runtime.onMessage.addListener(runtime_onMessage);
chrome.runtime.onConnectExternal.addListener(externalPort_onConnect);
