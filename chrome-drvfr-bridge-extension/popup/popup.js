
// message_status: normal, success, error

var onMessage_getPopupStatus = function (response) {
	if (response.data.status === 'error') {
		document.getElementById('btn-init-driver').classList.remove('hidden');
	} else {
		document.getElementById('btn-init-driver').classList.add('hidden');
	}
	document.getElementById('status-image').src = '../icons/status_' + response.data.status + '-128.png';
}
var onMessage_getDriverLog = function (response) {
	var log = response.data.log;
	var len = log.length;
	for (var i = 0; i < len; i++) {
		var msg = log[i];
		var el = document.getElementById('log-area');
		var html = el.innerHTML;
		var dt = new Date();
		el.innerHTML = 
			'<p class="message-' + msg.message_status + '">' + 
			dt.toLocaleDateString('ru-RU') + ' ' + dt.toLocaleTimeString('ru-RU') + ' ' +
			msg.message + 
			'</p>' + 
			html;
	}
}
var onMessage_driverInit = function () {
	//
}

var onMessage_pagesDisconnect = function (response) {
	var message_status = 'success';
	var dt = new Date();
	var el = document.getElementById('log-area');
	el.innerHTML = 
			'<p class="message-' + message_status + '">' + 
			dt.toLocaleDateString('ru-RU') + ' ' + dt.toLocaleTimeString('ru-RU') + ' ' +
			response.data.message + 
			'</p>';
}

var updateStatusImage = function () {
	chrome.runtime.sendMessage({ command: 'getPopupStatus' }, onMessage_getPopupStatus);
}

var updateDriverLog = function () {
	chrome.runtime.sendMessage({ command: 'getDriverLog' }, onMessage_getDriverLog);
}

var driverInit = function () {
	chrome.runtime.sendMessage({ command: 'driverInit' }, onMessage_driverInit);
}

var pagesDisconnect = function () {
	chrome.runtime.sendMessage({ command: 'pagesDisconnect' }, onMessage_pagesDisconnect);
}

document.addEventListener('DOMContentLoaded', function () {
	updateStatusImage();
	updateDriverLog();
	document.getElementById('btn-init-driver').onclick = driverInit;
	document.getElementById('btn-disconnect-pages').onclick = pagesDisconnect;
});